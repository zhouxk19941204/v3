export default {
  title: "职工管理",
  sort: 5,
  icon: "clarity:employee-group-solid",
  emoji: "👩🏼‍⚕️",
};
